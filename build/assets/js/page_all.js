/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
  //show menu
  $('.navbar-toggler').on('click', function () {
    $('.navbar-collapse,.navbar-modal').toggleClass('show');
  });
  $('.navbar-collapse .close,.navbar-modal').on('click', function () {
    $('.navbar-collapse,.navbar-modal').removeClass('show');
  }); // active navbar of page current

  var urlcurrent = window.location.href;
  $(".navbar-nav li a[href$='" + urlcurrent + "']").addClass('active'); // effect navbar

  $(window).scroll(function () {
    if ($(this).scrollTop() > 0) {
      $('header').addClass('scroll');
    } else {
      $('header').removeClass('scroll');
    }
  }); // loading done

  $(window).on("load", function (e) {
    $(".navbar-nav .sub-menu").parent("li").append("<span class='show-menu'></span>");
    $('.show-menu').on('click', function () {
      $(this).parent().addClass('show');
    });
  }); //

  var size_windown = $(window).innerWidth();

  if (size_windown < 1200) {
    $('.categories_menu a').on('click', function () {
      $('html,body').animate({
        scrollTop: $('.tab-content').offset().top
      }, 500);
    });
  } //modal bao gia


  $('.quotation .btn_download,.ads_list .btn_view').on('click', function () {
    $('#popup-quotation').modal('show');
  });
}); //scroll effect

$.fn.isInViewport = function () {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight() - 100;
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height() - 100;
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(window).on('resize scroll load', function () {
  $('.fadeup').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInUp').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadein').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeIn').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.zoomin').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('zoomIn').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadeinleft').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInLeft').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
  $('.fadeinright').each(function () {
    if ($(this).isInViewport()) {
      $(this).addClass('fadeInRight').css({
        'opacity': '1',
        'visibility': 'visible'
      });
    }
  });
});